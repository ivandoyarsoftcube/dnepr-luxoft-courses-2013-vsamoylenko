package com.luxoft.dnepr.courses.regular.unit2;

import com.luxoft.dnepr.courses.regular.unit2.unit2.CompositeProduct;
import com.luxoft.dnepr.courses.regular.unit2.unit2.ProductFactory;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class CompositeProductTest {
    private ProductFactory factory = new ProductFactory();

    @Test
    public void testAdd() {
        CompositeProduct compositeProduct = new CompositeProduct();

        assertEquals(0, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(1, compositeProduct.getAmount());

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(2, compositeProduct.getAmount());
    }

    @Test
    public void testGetPrice() {
        CompositeProduct compositeProduct = new CompositeProduct();

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals(10, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 10, true));
        assertEquals((10 + 10)* 0.95, compositeProduct.getPrice(), 0);

        compositeProduct.add(factory.createBeverage("cola", "Coca-cola", 20, true));
        assertEquals((10 + 10 + 20) * 0.9, compositeProduct.getPrice(), 0);
    }
}
