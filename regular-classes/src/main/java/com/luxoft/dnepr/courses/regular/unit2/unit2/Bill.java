package com.luxoft.dnepr.courses.regular.unit2.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {

    private Map<Product, CompositeProduct> product2SimilarProducts = new HashMap<Product, CompositeProduct>();

    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     *
     * @param product new product
     */
    //"TODO: implement me"
    public void append(Product product) {

    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     *
     * @return
     */
    //"TODO: implement me"
    public double summarize() {
        return 0.0;
    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     *
     * @return
     */
    //"TODO: implement me"
    public List<Product> getProducts() {
        return new ArrayList<Product>();
    }

    @Override
    //"TODO: implement me"
    public String toString() {
        return "";
    }

}
