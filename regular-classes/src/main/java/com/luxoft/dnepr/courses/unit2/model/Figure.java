package com.luxoft.dnepr.courses.unit2.model;

public abstract class Figure {
    //private double a;
    public abstract double calculateArea();
}

/*
public class Circle extends Figure {
    public double radius;
    public Circle(double radius){
        this.radius = radius;
    }
    @Override
    public double calculateArea() {
        return radius*radius*3.14;
    }
}

class Square  extends Figure {
    private double a;
    public Square(double a){
        this.a = a;
    }
    @Override
    public double calculateArea() {
        return a*a;
    }

}class Hexagon extends Figure {
    private double a;
    public Hexagon(double a){
        this.a = a;
    }
    @Override
    public double calculateArea() {
        return a*a*2.598076211354;
    }
}
*/
