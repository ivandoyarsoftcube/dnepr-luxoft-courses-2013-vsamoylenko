package com.luxoft.dnepr.courses.unit2.model;

public class Hexagon extends Figure {
    private double a;
    public Hexagon(double a){
        this.a = a;
    }
    @Override
    public double calculateArea() {
        return a*a*2.598076211354;
    }
}