package com.luxoft.dnepr.courses.unit1;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }


    public static String getMonthName(int monthOrder, String language) {

        if (language == "en") {
            switch (monthOrder) {
                case 1:
                    return "January";
                case 2:
                    return "February";
                case 3:
                    return "March";
                case 4:
                    return "April";
                case 5:
                    return "May";
                case 6:
                    return "June";
                case 7:
                    return "July";
                case 8:
                    return "Agust";
                case 9:
                    return "September";
                case 10:
                    return "October";
                case 11:
                    return "November";
                case 12:
                    return "December";

                default:
                    return "Unknown month";
            }
        }

            if (language == "ru") {
                switch (monthOrder) {
                    case 1:
                        return "Январь";
                    case 2:
                        return "Февраль";
                    case 3:
                        return "Март";
                    case 4:
                        return "Апрель";
                    case 5:
                        return "Май";
                    case 6:
                        return "Июнь";
                    case 7:
                        return "Июль";
                    case 8:
                        return "Август";
                    case 9:
                        return "Сентябрь";
                    case 10:
                        return "Октябрь";
                    case 11:
                        return "Ноябрь";
                    case 12:
                        return "Декабрь";
                    default:
                        return "Неизвестный месяц";
                }

            }
        if (language != "en" || language != "ru") {
            return "Unknown Language";
        }
        return "";
        }


    public static int pow(int a, int b) {
        int result = 1;
        for (int i = 0; i < b; i++) {
            result *= a;
        }
        return result;
    }
    public static String binaryToDecimal(String binaryNumber) {
        /*int binaryIntNubmer  =  Integer.parseInt(binaryNumber);
        int decimalIntNumber = binaryIntNubmer * 2;
        String decimalNumber = Integer.toString(decimalIntNumber);
        return decimalNumber;*/
        int res = 0;
        int n = binaryNumber.length() - 1;
        for (int i = 0; i <= n; i++) {
            int tmp = 0;
            char c = binaryNumber.charAt(i);
            //System.out.println(c);
            int a = Character.getNumericValue(c);
            //System.out.println(a);
            if ((a == 1) || (a == 0)) {
                tmp = a * pow(2, n - i);
                res += tmp;
            } else {
                return "Not binary";
            }
        }
        return Integer.toString(res);
    }



    public static String decimalToBinary(String decimalNumber) {
        StringBuilder bin = new StringBuilder();
        int dec;
        try {
            dec = Integer.parseInt(decimalNumber);
        }
        catch (NumberFormatException ex) {
            return  "Not decimal";
        }

        while (dec != 0) {
            bin.append(Integer.toString(dec % 2));
            dec /= 2;
        }
        return bin.reverse().toString();
    }




    public static int[] sortArray(int[] array, boolean asc) {
        int len = array.length;
        for (int i = 0; i < len - 1; i++) {
            int tmp;
            for (int j = 0; j < len - 1; j++) {
                if (asc) {
                    if (array[j] > array[j + 1]) {
                        tmp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = tmp;
                    }
                }
                else {
                    if (array[j] < array[j + 1]) {
                        tmp = array[j + 1];
                        array[j + 1] = array[j];
                        array[j] = tmp;
                    }
                }
            }
        }
        return array;
    }

}

