package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Figure;

import java.util.*;

public final class LuxoftUtils {

    private LuxoftUtils() {
    }

    /**
     * @param array String array
     * @param asc
     * @return sorted array in ascending order, if asc=<code>true</code>, otherwise in descending order.
     */

    public static String[] sortArray(String[] array, boolean asc) {

        String[] arrayCopy = array.clone();
        mergeSort(arrayCopy, asc);
        return arrayCopy;
    }

    private static void mergeSort(String[] array, boolean asc) {

        mergeSort(array, 0, array.length - 1, asc);
    }

    private static void mergeSort(String[] array, int left, int right, boolean asc) {

        if (left >= right) return;
        int m = (left + right) / 2;
        mergeSort(array, left, m, asc);
        mergeSort(array, m + 1, right, asc);
        merge(array, left, m, right, asc);
    }

    private static void merge(String[] array, int left, int middle, int right, boolean asc) {

        if (middle + 1 > right) return;
        String[] copyArray = copyToArray(array, left, middle, right);

        int k = left;
        int j = right;

        for (int i = left; i != right + 1; i++) {
            if (needToMergeLeft(copyArray, k, j, asc))
                array[i] = copyArray[k++];
            else
                array[i] = copyArray[j--];
        }

    }

    private static String[] copyToArray(String[] array, int left, int middle, int right) {

        String[] copyArray = new String[array.length];
        for (int i = left; i != middle + 1; i++) {
            copyArray[i] = array[i];
        }
        for (int i = middle + 1; i != right + 1; i++) {
            copyArray[i] = array[right + middle + 1 - i];
        }
        return copyArray;
    }

    private static boolean needToMergeLeft(String[] copyArray, int k, int j, boolean asc) {

        return (copyArray[k].compareTo(copyArray[j]) <= 0) == asc;
    }


    /**
     * Calculate the average length of word in str
     *
     * @param str String
     * @return wordAverageLengthInStr double
     */
    public static double wordAverageLength(String str) {
        String [] words = str.split(" ");
        float sumOfWords = 0;
        for (String word: words) {
            sumOfWords += word.length();
        }
        return (sumOfWords/words.length);
    }

    /**
     * Reverse all words in str
     *
     * @param str String
     * @return the newStr which contain all words in reverse order
     */
    public static String reverseWords(String str) {
        String[] words = str.split(" ");
        int lenOf = words.length;
        for (int i = 0; i < lenOf; i++) {
            words[i] = new StringBuilder(words[i]).reverse().toString();
        }
        String revWord ="";
        for (String word: words){
            revWord += word + " ";
        }
        return revWord;
    }



    /**
     * To get char array which contains the chars in order by their frequency
     *
     * @param str String
     * @return array char array
     */
    public static char[] getCharEntries(String str) {
        str = str.trim();
        Map<Character, Integer > states = new HashMap<Character, Integer>();
        for (int i = 0; i < str.length(); i++) {
            char charForCount = str.charAt(i);
            if (!states.containsKey(charForCount)) {
                int counter = 0;
                for (int j = 0; j < str.length(); j++) {
                    if (str.charAt(j) == charForCount){
                        counter++;
                    }
                }
                states.put(charForCount, counter);
            }
        }
        int sizeOfArr = states.size();
        char[] arrEntr = new char[sizeOfArr];
        ArrayList<Character> arrOfCharEntr = new ArrayList<Character>();
        StringBuilder sb = new StringBuilder();
        int index = 0;
        states.entrySet().stream()
                .sorted(Map.Entry.<Character, Integer>comparingByValue().reversed())
                .forEach(key -> sb.append(key.getKey()));

        for (int i = 0; i < sizeOfArr; i++) {
            arrEntr[i] = sb.charAt(i);
        }
        System.out.println(Arrays.toString(arrEntr));
        return arrEntr;
    }



    /**
     * To calculate sum of area of the figures
     *
     * @param figures List<Figure>
     * @return sumOfArea double
     */
    public static double calculateOverallArea(List<Figure> figures) {
        double sum = 0;
        for (Figure figure : figures){
            sum += figure.calculateArea();
        }
        return sum;
    }
}
