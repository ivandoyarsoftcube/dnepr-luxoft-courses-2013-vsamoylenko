package com.luxoft.dnepr.courses.regular.unit2.unit2;

public class Beverage extends AbstractProduct implements Product {

    private boolean nonAlcoholic;

    public Beverage(String code, String name, double price, boolean nonAlcoholic) {
        super(code, name, price);
        this.nonAlcoholic = nonAlcoholic;
    }
}
