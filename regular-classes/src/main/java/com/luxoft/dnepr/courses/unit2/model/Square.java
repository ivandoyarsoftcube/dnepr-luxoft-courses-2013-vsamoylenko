package com.luxoft.dnepr.courses.unit2.model;

public class Square  extends Figure {
    private double a;

    public Square(double a) {
        this.a = a;
    }

    @Override
    public double calculateArea() {
        return a * a;
    }
}