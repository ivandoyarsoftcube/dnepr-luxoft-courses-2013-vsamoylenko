package com.luxoft.dnepr.courses.regular.unit2.unit2;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents group of similar {@link Product}s.
 * Implementation of pattern Composite.
 */
public class CompositeProduct implements Product {

    private List<Product> childProducts;

    public CompositeProduct(){
        childProducts = new ArrayList<Product>();
    }

    public CompositeProduct(Product product){
        this();
        childProducts.add(product);
    }

    /**
     * Returns code of the first "child" product or null, if child list is empty
     *
     * @return product code
     */
    @Override
    //"TODO: implement me"
    public String getCode() {
        return "";
    }

    /**
     * Returns name of the first "child" product or null, if child list is empty
     *
     * @return product name
     */
    @Override
    //"TODO: implement me"
    public String getName() {
        return "";
    }

    /**
     * Returns total price of all the child products taking into account discount.
     * 1 item - no discount
     * 2 items - 5% discount
     * >= 3 items - 10% discount
     *
     * @return total price of child products
     */
    @Override
    //"TODO: implement me"
    public double getPrice() {
        return 0.0;
    }

    //"TODO: implement me"
    public int getAmount() {
        return 0;
    }

    //"TODO: implement me"
    public void add(Product product) {

    }

    //"TODO: implement me"
    public void remove(Product product) {

    }

    @Override
    public String toString() {
        return getName() + " * " + getAmount() + " = " + getPrice();
    }
}
