package com.luxoft.dnepr.courses.regular.unit2.unit2;

public class Bread extends AbstractProduct implements Product {

    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code, name, price);
        this.weight = weight;
    }
}
