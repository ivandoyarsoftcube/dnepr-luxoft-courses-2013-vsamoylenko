package com.luxoft.dnepr.courses.unit2.model;

public class Circle extends Figure {
    private double radius;
    public Circle(double radius){
        this.radius = radius;
    }
    @Override
    public double calculateArea() {
        return radius*radius*3.1415926535897932384626433832795;
    }

    /*public static void main(String[] args) {
        Circle cr = new Circle(1);
        System.out.println(cr.calculateArea());
    }*/
}
