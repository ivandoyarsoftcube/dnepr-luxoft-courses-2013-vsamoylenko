package com.luxoft.dnepr.courses.regular.unit2.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Product, Cloneable {

    private Date publicationDate;

    public Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate = publicationDate;
    }
}
